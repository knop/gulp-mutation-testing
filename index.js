var through = require('through2');
var gutil = require('gulp-util');
var PluginError = gutil.PluginError;
var profx = require('./profx');
const PLUGIN_NAME = 'gulp-mutation-testing';

function prefixStream(prefixText) {
    var stream = through();
    stream.write(prefixText);
    return stream;
}

function gulpMutationTesting(src) {
    gutil.log('!!src', src);
    if (!src) {
        throw new PluginError(PLUGIN_NAME, 'Missing sources!');
    }
    // Creating a stream through which each file will pass
    return through.obj(function(file, enc, cb) {

        if (file.isNull()) {
            return cb(null, file);
        }

        profx(src, file.path, function(err, score) {
            console.log('score', score);
            cb(null, file);
        });

    });

}

// Exporting the plugin main function
module.exports = gulpMutationTesting;